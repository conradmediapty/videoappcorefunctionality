package com.nick.sampleffmpeg.ui.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;

import com.nick.sampleffmpeg.Define.Constant;
import com.nick.sampleffmpeg.MainApplication;
import com.nick.sampleffmpeg.bean.OverlayBean;
import com.nick.sampleffmpeg.utils.AppConstants;
import com.nick.sampleffmpeg.utils.BitmapUtils;
import com.nick.sampleffmpeg.utils.FontTypeface;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

/**
 * Created by baebae on 2/9/16.
 */
public class ThumbnailView extends View {
    private Context appContext;
    private int _width;
    private int _height;
    private double currentVideoTime = 0;

    private Bitmap viewBitmap = null;
    private Canvas viewCanvas;
    private Paint mBitmapPaint;
    public String strDirectoryID = "";

    private class ThumbnailInformation {
        public double x = 0;
        public double y = 0;
        public double width = 0;
        public double height = 0;

        public String fontName = "";
        public String strText = "";
        public double fontSize = 0;
        public int color = Color.argb(0, 255, 255, 255);

        public String fileName = "";
    }
    private void init()
    {
        if (!isInEditMode()) {
            setWillNotDraw(false);
            mBitmapPaint = new Paint(Paint.DITHER_FLAG);
        }
    }

    public ThumbnailView(Context context) {
        super(context);
        this.appContext = context;
        if (!isInEditMode()) {
            init();
        }
    }

    public ThumbnailView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        this.appContext = context;
        init();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh)
    {
        super.onSizeChanged(w, h, oldw, oldh);
        if (!isInEditMode()) {
            _width = w;
            _height = h;
            viewBitmap = Bitmap.createBitmap(_width, _height, Bitmap.Config.ARGB_8888);
            viewCanvas = new Canvas(viewBitmap);
        }
    }

    private ThumbnailInformation brandInfo = null;
    private ThumbnailInformation playInfo = null;
    private ThumbnailInformation textInfo = null;
    private ThumbnailInformation profileInfo = null;
    private ThumbnailInformation backgroundInfo = null;
    private ThumbnailInformation jobtitleInfo = null;

    public Bitmap initializeView(String jobTitle) {
        brandInfo = getThumbnailInformation("brand");
        brandInfo.fileName = Constant.getApplicationDirectory() + strDirectoryID + File.separator + "brand.png";

        playInfo = getThumbnailInformation("playbutton");
        playInfo.fileName = Constant.getOverlayDirectory() + File.separator + "app_files" + File.separator + "app_play_icon.png";

        textInfo = getThumbnailInformation("text");

        profileInfo = getThumbnailInformation("displaypicture");
        profileInfo.fileName = Constant.getApplicationDirectory() + strDirectoryID + File.separator + "youtube_thumbnail.jpg";

        backgroundInfo = getThumbnailInformation("background");

        jobtitleInfo = getThumbnailInformation("jobtitle");
        jobtitleInfo.strText = jobTitle;

        invalidate();
        Constant.setThumbnailImageUrl(Constant.getOverlayDirectory() + "Thumbnail.png");
        return saveToFile(Constant.getThumbnailImageUrl(), 1920, 1080);
    }

    private void drawBackground(Canvas canvas, int width, int height) {
        Bitmap background = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        background.eraseColor(backgroundInfo.color);
        canvas.drawBitmap(background, 0, 0, mBitmapPaint);
    }

    private void drawBrandInfo(Canvas canvas, int width, int height) {
        int x = (int)(width * (brandInfo.x / 100.f));
        int y = (int)(height * (brandInfo.y / 100.f));

        int newWidth = (int)(width * (brandInfo.width / 100.f));
        int newHeight = (int)(height * (brandInfo.height / 100.f));

        Bitmap fullBitmap = BitmapUtils.getBitmapFromPNGFile(brandInfo.fileName);
        if (fullBitmap != null) {
            Bitmap resizedBitmap = BitmapUtils.getResizedBitmap(fullBitmap, newWidth, newHeight);
            canvas.drawBitmap(resizedBitmap, x, y, mBitmapPaint);
        }

    }

    private void drawProfileImage(Canvas canvas, int width, int height) {
        Bitmap fullBitmap = BitmapUtils.getBitmapFromPNGFile(profileInfo.fileName);
        if (fullBitmap != null) {
            Bitmap roundedBitmap = getCircleBitmap(fullBitmap);

            int x = (int)(width * (profileInfo.x / 100.f));
            int y = (int)(height * (profileInfo.y / 100.f));

            int newWidth = (int)(width * (profileInfo.width / 100.f));
            int newHeight = (int)(height * (profileInfo.height / 100.f));

            Bitmap resizedBitmap = BitmapUtils.getResizedBitmap(roundedBitmap, newWidth, newHeight);
            canvas.drawBitmap(resizedBitmap, x, y, mBitmapPaint);
        }

    }

    private void drawPlayImage(Canvas canvas, int width, int height) {
        Bitmap playBitmap = BitmapUtils.getBitmapFromPNGFile(playInfo.fileName);
        int x = (int)(width * (playInfo.x / 100.f));
        int y = (int)(height * (playInfo.y / 100.f));
        int newWidth = (int)(width * 0.15);

        Bitmap resizedBitmap = BitmapUtils.getResizedBitmap(playBitmap, newWidth, newWidth);
        canvas.drawBitmap(resizedBitmap, x, y, mBitmapPaint);
    }

    private void drawText(Canvas canvas, ThumbnailInformation textInfo, int width, int height) {
        TextPaint mTextPaint = new TextPaint();
        mTextPaint.setColor(textInfo.color);


        int marginLeft = (int) ((textInfo.x / 100) * width) ;
        int marginTop = (int)((textInfo.y / 100) * height);
        int fontSize = (int) (((double)textInfo.fontSize / 360) * height) ;

        mTextPaint.setTextSize(fontSize);
        mTextPaint.setTextAlign(Paint.Align.CENTER);
        if (textInfo.fontName.length() > 0) {
            Typeface typeFace = FontTypeface.getTypeface(appContext, textInfo.fontName);
            mTextPaint.setTypeface(typeFace);
        }
        mTextPaint.setAntiAlias(true);
        drawMultilineText(textInfo.strText, marginLeft, marginTop, mTextPaint, canvas, fontSize, width);
    }

    void drawMultilineText(String str, int x, int y, Paint paint, Canvas canvas, int fontSize, int width) {
        int      lineHeight = 0;
        int      yoffset    = 0;
        String[] lines      = str.split("\n");

        Rect mBounds = new Rect();
        paint.getTextBounds("Ig", 0, 2, mBounds);
        lineHeight = (int) ((float) mBounds.height() * 1.5);
        for (int i = 0; i < lines.length; ++i) {
            canvas.drawText(lines[i], (width - mBounds.width()) / 2, y + yoffset + fontSize, paint);
            yoffset = yoffset + lineHeight;
        }
    }

    public Bitmap saveToFile(String fileName, int width, int height) {
        Bitmap bmOverlay = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bmOverlay);
        drawToCanvas(canvas, width, height);
        BitmapUtils.saveBitmapToFile(bmOverlay, fileName);
        return bmOverlay;
    }

    public void drawToCanvas(Canvas canvas, int width, int height) {
        if (backgroundInfo != null) {
            drawBackground(canvas, width, height);
        }

        if (brandInfo != null) {
            drawBrandInfo(canvas, width, height);
        }

        if (profileInfo != null) {
            drawProfileImage(canvas, width, height);
        }

        if (playInfo != null) {
            drawPlayImage(canvas, width, height);
        }

        if (textInfo != null && textInfo.strText.length() > 0) {
            drawText(canvas, textInfo, width, height);
        }

        if (jobtitleInfo != null && jobtitleInfo.strText.length() > 0) {
            drawText(canvas, jobtitleInfo, width, height);
        }
    }
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawToCanvas(canvas, _width, _height);
    }

    private Bitmap getCircleBitmap(Bitmap bitmap) {
        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        bitmap.recycle();

        return output;
    }

    private String getFontFileName(String fontName) {
        String lowerCaseFontName = fontName.toLowerCase();
        for (int i = 0; i < AppConstants.CAPTION_FONTS.length; i ++) {
            AppConstants.FontMatch font = AppConstants.CAPTION_FONTS[i];

            String lowerMatchName = font.fontTypeName.toLowerCase();
            if (lowerCaseFontName.contains(lowerMatchName)) {
                return font.fontFileName;
            }
        }

        return  "";
    }

    public JSONObject getTemplateJSONObject() {
        JSONObject ret = null;
        try {
            JSONObject jsonObject = MainApplication.getInstance().getTemplateArray().getJSONObject(MainApplication.getInstance().getSelectedTemplePosition());

            if (!jsonObject.isNull("directory")) {
                strDirectoryID = jsonObject.getString("directory");
            }

            if (!jsonObject.isNull("data")) {
                if (!jsonObject.getJSONObject("data").isNull("youtube")) {
                    ret = jsonObject.getJSONObject("data").getJSONObject("youtube");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

    public ThumbnailInformation getThumbnailInformation(String type) {
        ThumbnailInformation ret = null;
        ThumbnailInformation info = new ThumbnailInformation();

        try {
            JSONObject thumbInfo = getTemplateJSONObject().getJSONObject(type);
            if (!thumbInfo.isNull("x")) {
                info.x = thumbInfo.getDouble("x");
            }

            if (!thumbInfo.isNull("y")) {
                info.y = thumbInfo.getDouble("y");
            }

            if (!thumbInfo.isNull("w")) {
                info.width = thumbInfo.getDouble("w");
            }

            if (!thumbInfo.isNull("h")) {
                info.height = thumbInfo.getDouble("h");
            }

            if (!thumbInfo.isNull("color")) {
                String color = thumbInfo.getString("color");
                if (color.length() == 4) {
                    color = "#" + color.charAt(1) + color.charAt(1) + color.charAt(2) + color.charAt(2) + color.charAt(3) + color.charAt(3);
                }
                info.color = Color.parseColor(color);
            }

            if (!thumbInfo.isNull("text")) {
                info.strText = thumbInfo.getString("text");
            }

            if (!thumbInfo.isNull("size")) {
                info.fontSize = thumbInfo.getInt("size");
            }

            if (!thumbInfo.isNull("font")) {
                String fontName = thumbInfo.getString("font");
                info.fontName = getFontFileName(fontName);
            }
            ret = info;
        } catch (Exception e) {

        }

        return ret;
    }
}
