package com.nick.sampleffmpeg.utils.youtube.util;

import android.content.Context;
import android.content.Intent;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.java6.auth.oauth2.FileCredentialStore;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.googleapis.media.MediaHttpUploader;
import com.google.api.client.googleapis.media.MediaHttpUploaderProgressListener;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.InputStreamContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.ThumbnailSetResponse;
import com.google.common.collect.Lists;
import com.nick.sampleffmpeg.Define.Constant;
import com.nick.sampleffmpeg.ui.activity.UploadingVideoScreen;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

/**
 * Created by Admin on 2/14/2016.
 */



public class UploadThumbnail {

    static  String  videoID  ="";
   static String accessTocken = "";







    /**
     * Global instance of the HTTP transport.
     */
    private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();

    /**
     * Global instance of the JSON factory.
     */
    private static final JsonFactory JSON_FACTORY = new JacksonFactory();
   static GoogleCredential credential = null;

    /**
     * Global instance of Youtube object to make all API requests.
     */
    private static YouTube youtube;

    /* Global instance of the format used for the image being uploaded (MIME type). */
    private static String IMAGE_FILE_FORMAT = "image/png";



    /**
     * Authorizes the installed application to access user's protected data.
     *
     * @param scopes list of scopes needed to run youtube upload.
     */
    private static Credential authorize(List<String> scopes) throws Exception {




        credential = new GoogleCredential.Builder()
                .setTransport(com.nick.sampleffmpeg.ui.activity.Auth.HTTP_TRANSPORT).setJsonFactory(com.nick.sampleffmpeg.ui.activity.Auth.JSON_FACTORY)
                .setClientSecrets("815107145608-2hc3kfand4bomob5thte673amk17k4c2.apps.googleusercontent.com", "46ZZiJ5z01zj7Lgoz9f35Fd0").build();
        credential.setAccessToken(accessTocken);












        // Authorize.
        return credential;
    }


    public static void  main(String videoIDLocal , String accessTockenlocal , Context context) {


        videoID  = videoIDLocal;

        accessTocken =accessTockenlocal;




        // An OAuth 2 access scope that allows for full read/write access.
        List<String> scopes = Lists.newArrayList("https://www.googleapis.com/auth/youtube");

        try {
            // Authorization.
            Credential credential = authorize(scopes);

            // YouTube object used to make all API requests.
            youtube = new YouTube.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).setApplicationName(
                    "VideoMyJob").build();

            // Get the user selected video Id.
            String videoId = videoID;
            System.out.println("You chose " + videoId + " to upload a thumbnail.");

            // Get the user selected local image file to upload.
            File imageFile  = new File(Constant.getThumbnailImageUrl());
            ;
            System.out.println("You chose " + imageFile + " to upload.");

            InputStreamContent mediaContent = new InputStreamContent(
                    IMAGE_FILE_FORMAT, new BufferedInputStream(new FileInputStream(imageFile)));
            mediaContent.setLength(imageFile.length());

            // Create a request to set the selected mediaContent as the thumbnail of the selected video.
            YouTube.Thumbnails.Set thumbnailSet = youtube.thumbnails().set(videoId, mediaContent);

            // Set the upload type and add event listener.
            MediaHttpUploader uploader = thumbnailSet.getMediaHttpUploader();

      /*
       * Sets whether direct media upload is enabled or disabled. True = whole media content is
       * uploaded in a single request. False (default) = resumable media upload protocol to upload
       * in data chunks.
       */
            uploader.setDirectUploadEnabled(false);

            MediaHttpUploaderProgressListener progressListener = new MediaHttpUploaderProgressListener() {
                @Override
                public void progressChanged(MediaHttpUploader uploader) throws IOException {
                    switch (uploader.getUploadState()) {
                        case INITIATION_STARTED:
                            System.out.println("Initiation Started");
                            break;
                        case INITIATION_COMPLETE:
                            System.out.println("Initiation Completed");
                            break;
                        case MEDIA_IN_PROGRESS:
                            System.out.println("Upload in progress");
                            System.out.println("Upload percentage: " + uploader.getProgress());
                            break;
                        case MEDIA_COMPLETE:
                            System.out.println("Upload Completed!");
                            break;
                        case NOT_STARTED:
                            System.out.println("Upload Not Started!");
                            break;
                    }
                }
            };
            uploader.setProgressListener(progressListener);

            // Execute upload and set thumbnail.
            ThumbnailSetResponse setResponse = thumbnailSet.execute();

            // Print out returned results.
            System.out.println("\n================== Uploaded Thumbnail ==================\n");
            System.out.println("  - Url: " + setResponse.getItems().get(0).getDefault().getUrl());

        if(setResponse.getItems().get(0).getDefault().getUrl()==null || setResponse.getItems().get(0).getDefault().getUrl().equalsIgnoreCase("")){

            Intent intent = new Intent("THUMBNIAL_CANCELED");
            context.sendBroadcast(intent);
        }


        } catch (GoogleJsonResponseException e) {
            System.err.println("GoogleJsonResponseException code: " + e.getDetails().getCode() + " : "
                    + e.getDetails().getMessage());
            e.printStackTrace();
            Intent intent = new Intent("THUMBNIAL_CANCELED");
            context.sendBroadcast(intent);
        } catch (Exception e) {
            System.err.println("IOException: " + e.getMessage());
            e.printStackTrace();
            Intent intent = new Intent("THUMBNIAL_CANCELED");
            context.sendBroadcast(intent);
        }
    }





}