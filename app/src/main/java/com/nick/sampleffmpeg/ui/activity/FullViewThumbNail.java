package com.nick.sampleffmpeg.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ImageView;

import com.nick.sampleffmpeg.Define.Constant;
import com.nick.sampleffmpeg.R;
import com.nick.sampleffmpeg.ui.control.UITouchButton;
import com.nick.sampleffmpeg.ui.view.ThumbnailView;

import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by Admin on 1/31/2016.
 */
public class FullViewThumbNail extends Activity {

    private ThumbnailView thumbnailView;
    private ImageView imageThumbView;
    private View btn_back;
    private View btnCancel;
    PhotoViewAttacher mAttacher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //   setContentView(R.la);
        setContentView(R.layout.thumbnail_full_screen);

        Intent intent = getIntent();
        String strJobTitle = intent.getStringExtra("VideoTitle");


        thumbnailView = (ThumbnailView)findViewById(R.id.thumbView);
        Bitmap bmpThumb = thumbnailView.initializeView(strJobTitle);

        imageThumbView = (ImageView)findViewById(R.id.imageThumbView);
        imageThumbView.setImageBitmap(bmpThumb);

        mAttacher = new PhotoViewAttacher(imageThumbView);

        btn_back = findViewById(R.id.btn_back);
        btnCancel = findViewById(R.id.btnCancel);

        UITouchButton.applyEffect(btn_back, UITouchButton.EFFECT_ALPHA, Constant.BUTTON_NORMAL_ALPHA,
                Constant.BUTTON_FOCUS_ALPHA, new Runnable() {
                    @Override
                    public void run() {
                        FullViewThumbNail.this.finish();
                    }
                });

        UITouchButton.applyEffect(btnCancel, UITouchButton.EFFECT_ALPHA, Constant.BUTTON_NORMAL_ALPHA,
                Constant.BUTTON_FOCUS_ALPHA, new Runnable() {
                    @Override
                    public void run() {
                        FullViewThumbNail.this.finish();
                    }
                });
    }

}
